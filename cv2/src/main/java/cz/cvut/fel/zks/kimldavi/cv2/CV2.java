package cz.cvut.fel.zks.kimldavi.cv2;

public class CV2 {
    public int function1(int x1, int x2, int x3)
    {
        int value = 0;
        if (x1 > 4) {
            if (x2 < 5 && x3 < 10) {
                value = x2;
            } else {
                value = x3;
            }
            return value * 2;
        } else {
            value = x1;
            return value;
        }
    }
}
