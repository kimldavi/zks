package cz.cvut.fel.zks.kimldavi.cv2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CV2Test {

    @Test
    void function1Test(){
        int x1 = 5, x2 = 4, x3 = 10;
        CV2 cv2 = new CV2();

        int result = cv2.function1(x1,x2,x3);

        Assertions.assertEquals(20, result);
    }
}
